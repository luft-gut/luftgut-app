import 'package:flutter/material.dart';
import 'package:luftgut/services/firebase/database.dart';
import 'package:weather_icons/weather_icons.dart';

class WeatherCard extends StatelessWidget {
  WeatherCard({this.weatherText, this.capability});

  final String weatherText;
  final Capability capability;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
      child: ListTile(
        leading: getIcon(capability.id),
        title: Text(
          '$weatherText ${capability.unit}',
          style: TextStyle(
            color: Colors.teal[900],
            // fontFamily: 'SourceSansPro',
            fontSize: 20.0,
          ),
        ),
      ),
    );
  }
}

Icon getIcon(String capability) {
  final Color iconColor = Colors.teal[900];

  Icon icon = Icon(Icons.close);
  switch (capability) {
    case 'temperature':
      {
        icon = Icon(
          WeatherIcons.thermometer,
          color: iconColor,
        );
      }
      break;
    case 'pressure':
      {
        icon = Icon(
          WeatherIcons.barometer,
          color: iconColor,
        );
      }
      break;
    case 'humidity':
      {
        icon = Icon(
          WeatherIcons.humidity,
          color: iconColor,
        );
      }
      break;
    case 'airSpeed':
      {
        icon = Icon(
          WeatherIcons.strong_wind,
          color: iconColor,
        );
      }
      break;
    case 'location':
      {
        icon = Icon(
          Icons.location_pin,
          color: iconColor,
        );
      }
      break;
    case 'windDirection':
      {
        icon = Icon(
          WeatherIcons.wind,
          color: iconColor,
        );
      }
      break;
  }
  return icon;
}
