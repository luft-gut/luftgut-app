import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:luftgut/screens/choose_station_screen.dart';
import 'package:luftgut/screens/welcome_screen.dart';
import 'package:luftgut/screens/login_screen.dart';
import 'package:luftgut/screens/registration_screen.dart';
import 'package:luftgut/screens/actual_weather_screen.dart';
import 'package:luftgut/screens/reports_screen.dart';
import 'package:luftgut/screens/showReport_screen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(Luftgut());
}

class Luftgut extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      initialRoute: WelcomeScreen.id,
      routes: {
        WelcomeScreen.id: (context) => WelcomeScreen(),
        LoginScreen.id: (context) => LoginScreen(),
        RegistrationScreen.id: (context) => RegistrationScreen(),
        ChooseStationScreen.id: (context) => ChooseStationScreen(),
        ActualWeather.id: (context) => ActualWeather(),
        ReportsScreen.id: (context) => ReportsScreen(),
        ShowReportScreen.id: (context) => ShowReportScreen(),
      },
    );
  }
}
