import 'package:luftgut/services/openWeather/apiNetworking.dart';

const apiKey = 'abe28e4a842477dc2ffe5d855fb93ae4';
const openWeatherMapURL = 'https://api.openweathermap.org/data/2.5/weather';

class WeatherModel {

  Future<dynamic> getCityWeather(String cityName) async {

    NetworkHelper networkHelper = NetworkHelper(
        '$openWeatherMapURL?q=$cityName&appid=$apiKey&units=metric');

    var weatherData = await networkHelper.getData();

    return weatherData;

  }

  String getWeatherIcon(int condition, int sunrise, int sunset, int now) {

    var nowDate = DateTime.fromMillisecondsSinceEpoch(now*1000, isUtc: true);
    var sunriseDate = DateTime.fromMillisecondsSinceEpoch(sunrise*1000, isUtc: true);
    var sunsetDate = DateTime.fromMillisecondsSinceEpoch(sunset*1000, isUtc: true);

    if (condition < 300) {
      return '🌩';
    } else if (condition < 400) {
      return '🌧';
    } else if (condition < 600) {
      return '☔️';
    } else if (condition < 700) {
      return '☃️';
    } else if (condition < 800) {
      return '🌫';
    } else if (condition == 800) {
      if(nowDate.isAfter(sunriseDate) && nowDate.isBefore(sunsetDate)){
        return '☀️';
      } else{
        return '🌙';
      }
    } else if (condition <= 804) {
      return '☁️';
    } else {
      return '🤷‍';
    }
  }

  String getAnimation(int condition, int sunrise, int sunset, int now) {

    var nowDate = DateTime.fromMillisecondsSinceEpoch(now*1000, isUtc: true);
    var sunriseDate = DateTime.fromMillisecondsSinceEpoch(sunrise*1000, isUtc: true);
    var sunsetDate = DateTime.fromMillisecondsSinceEpoch(sunset*1000, isUtc: true);

    if (condition < 300) {
      return 'images/storm.json';
    } else if (condition < 400) {
      return 'images/rain-cloud.json';
    } else if (condition < 600) {
      return 'images/rain.json';
    } else if (condition < 700) {
      return 'images/light--snow.json';
    } else if (condition < 800) {
      return 'images/windy-weather.json';
    } else if (condition == 800) {
      if(nowDate.isAfter(sunriseDate) && nowDate.isBefore(sunsetDate)){
        return 'images/sun.json';
      } else{
        return 'images/moon.json';
      }
    } else if (condition <= 804) {
      return 'images/cloudy.json';
    } else {
      return 'images/barometer.json‍';
    }
  }



}