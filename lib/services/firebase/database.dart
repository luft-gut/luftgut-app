import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Capability {
  final String id;
  final Map<String, dynamic> name;
  final String iconString;
  final String type;
  final String unit;
  final Icon icon;

  Capability(
      {this.type, this.name, this.id, this.iconString, this.unit, this.icon});

  factory Capability.fromFirestore(DocumentSnapshot doc) {
    Map data = doc.data();
    return Capability(
        id: doc.id,
        name: data['name'] ?? '',
        iconString: data['icon'] ?? '',
        type: data['type'] ?? '',
        unit: data['unit'] ?? '');
  }
}

class Datapoint {
  final String id;
  final Map<String, dynamic> data;
  final Timestamp timestamp;

  Datapoint({this.id, this.timestamp, this.data});

  factory Datapoint.fromFirestore(QuerySnapshot snap) {
    var datapoints = snap.docs;

    return Datapoint(
        id: datapoints[0].id ?? '',
        data: datapoints[0].data()['data'] ?? '0',
        timestamp: datapoints[0]['timestamp'] ?? DateTime.now());
  }
}

class Station {
  final String id;
  final List<dynamic> capabilityIdList;
  final String location;
  final String ownerId;
  final bool online;

  Station(
      {this.id,
      this.location,
      this.ownerId,
      this.online,
      this.capabilityIdList});

  factory Station.fromFirestore(DocumentSnapshot doc) {
    Map data = doc.data();

    return Station(
        id: doc.id,
        capabilityIdList: data['capabilities'],
        location: data['location'] ?? '',
        ownerId: data['owner'] ?? '',
        online: data['online'] ?? false);
  }
}

class Report {
  final Timestamp timeStamp;
  final magnitude;

  Report({this.timeStamp, this.magnitude});

  factory Report.fromMap(DocumentSnapshot doc, String capability){
    Map data = doc.data();
    return Report(
        timeStamp: data['timestamp'] ?? '',
        magnitude: data['data'][capability]
    );
  }
}