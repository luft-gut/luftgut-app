import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:luftgut/services/firebase/database.dart';

final _fireStore = FirebaseFirestore.instance;

class StreamReports extends StatelessWidget {

  StreamReports({this.station, this.capability});
  final Station station ;
  final Capability capability;

  List<Report> myData;
  List<charts.Series<Report,DateTime>> _reportData;
  _generateData(myData) {
    _reportData = List<charts.Series<Report, DateTime>>();
      _reportData.add(
          charts.Series(
              domainFn: (Report weatherInformation, _) =>
                  weatherInformation.timeStamp.toDate(),
              measureFn: (Report weatherInformation,
                  _) => weatherInformation.magnitude,
              id: capability.id,
              data: myData
          )
      );
  }

  Widget _buildChart (BuildContext context, List<Report> weatherdata){
    myData = weatherdata;
    _generateData(myData);
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 100.0, horizontal: 10.0),
      child: Container(
        child: Center(
          child: Column(
            children: <Widget>[
              Text(capability.name['de']),
              SizedBox(height: 10.0),
              Expanded(
                child: charts.TimeSeriesChart(_reportData),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: _fireStore.collection('stations')
          .doc(station.id)
          .collection('datapoints')
          .orderBy('timestamp', descending: true)
          .limit(100)
          .snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(
              backgroundColor: Colors.teal[300],
            ),
          );
        } else {
          List<Report> weatherInformations = snapshot.data.docs
              .map((documentSnapshot) =>
                  Report.fromMap(documentSnapshot, capability.id))
              .toList();
          return _buildChart(context, weatherInformations);
        }
      },
    );
  }
}
