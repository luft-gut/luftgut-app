import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:luftgut/services/firebase/database.dart';

class DatabaseService {
  final FirebaseFirestore _db = FirebaseFirestore.instance;
  Stream<List<Capability>> streamCapabilitiyList(){
    return _db.collection('capabilities').snapshots().map((list) => list.docs.map((doc) => Capability.fromFirestore(doc)).toList());
  }

  Stream<List<Station>> streamStations(User user) {
    return _db
        .collection('stations')
        .where('owner', isEqualTo: user.uid)
        .snapshots()
        .map((list) =>
            list.docs.map((doc) => Station.fromFirestore(doc)).toList());
  }

  Stream<Datapoint> streamDatapoint(Station station) {
    var ref =
        _db.collection('stations').doc(station.id).collection('datapoints').orderBy('timestamp', descending: true);
    return ref.snapshots().map((snap) => Datapoint.fromFirestore(snap));
  }

}

