import 'package:flutter/material.dart';
import 'package:luftgut/components/rounded_Button.dart';
import 'package:luftgut/screens/showReport_screen.dart';
import 'package:luftgut/services/firebase/database.dart';

class ReportsScreen extends StatefulWidget {
  static const String id = 'reports_screen';

  ReportsScreen({this.station, this.mapCapability, this.datapoint});

  final Station station;
  final Datapoint datapoint;
  final mapCapability;

  @override
  _ReportsScreenState createState() => _ReportsScreenState(
      station: station, mapCapability: mapCapability, datapoint: datapoint);
}

class _ReportsScreenState extends State<ReportsScreen> {
  _ReportsScreenState({this.station, this.mapCapability, this.datapoint});

  final Station station;
  final Datapoint datapoint;
  final mapCapability;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: null,
        title: Text('⛅ Luftgut'),
        backgroundColor: Colors.teal[300],
      ),
      backgroundColor: Colors.teal[150],
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(
              height: 100.0,
            ),
            CapabilitiesCards(
              station: station,
              mapCapability: mapCapability,
              datapoint: datapoint,
            ),
          ],
        ),
      ),
    );
  }
}

class CapabilitiesCards extends StatelessWidget {
  CapabilitiesCards({this.station, this.mapCapability, this.datapoint});

  final Station station;
  final Map<String, Capability> mapCapability;
  final Datapoint datapoint;

  @override
  Widget build(BuildContext context) {
    List<RoundedButton> roundedButtonList = [];
    mapCapability.forEach((key, capability) {
      if (capability.type == 'Number') {
        final roundedButton = RoundedButton(
            buttonColor: Colors.teal[300],
            buttonText: capability.name['de'],
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ShowReportScreen(
                    station: station,
                    capability: capability,
                  ),
                ),
              );
            });
        roundedButtonList.add(roundedButton);
      }
    });
    /*
    //for (String key in datapoint.data.keys) {
    for (String key in mapCapability.keys) {
      Capability capability = mapCapability[key];
      if (capability.type == 'Number') {
        final roundedButton = RoundedButton(
            buttonColor: Colors.teal[300],
            buttonText: capability.name['de'],
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ShowReportScreen(
                    station: station,
                    capability: capability,
                  ),
                ),
              );
            });
        roundedButtonList.add(roundedButton);
      }
    }
*/
    return Expanded(
      child: ListView(
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
        children: roundedButtonList,
      ),
    );
  }
}
