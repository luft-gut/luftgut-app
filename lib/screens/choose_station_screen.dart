import 'dart:isolate';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:luftgut/components/rounded_Button.dart';
import 'package:luftgut/screens/actual_weather_screen.dart';
import 'package:luftgut/services/firebase/database.dart';
import 'package:luftgut/services/firebase/databaseService.dart';
import 'package:luftgut/services/openWeather/weatherAnimation.dart';
import 'dart:async';

final _auth = FirebaseAuth.instance;
User loggedInUser;
final db = DatabaseService();
final Map<String, Capability> mapCapability = {};

class ChooseStationScreen extends StatefulWidget {
  static const String id = 'choose_station_screen';

  @override
  _ChooseStationScreenState createState() => _ChooseStationScreenState();
}

class _ChooseStationScreenState extends State<ChooseStationScreen> {
  Future<Map<Station, Datapoint>> mapSD;

  void getCurrentUser() {
    try {
      final user = _auth.currentUser;
      if (user != null) {
        loggedInUser = user;
      }
    } catch (e) {
      print(e);
    }
  }

  void getCapabilities() async {
    mapCapability.clear();
    final capList = await FirebaseFirestore.instance
        .collection('capabilities')
        .get()
        .then((capList) => capList.docs.forEach((cap) {
              print(cap.id);
              mapCapability.putIfAbsent(
                  cap.id, () => Capability.fromFirestore(cap));
            }));
  }

  @override
  void initState() {
    super.initState();
    getCurrentUser();
    getCapabilities();
    print('Map Capbility');
    mapCapability.forEach((key, value) {
      print(key);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: null,
        title: Text('⛅ Luftgut'),
        backgroundColor: Colors.teal[300],
      ),
      backgroundColor: Colors.white,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(
              height: 48.0,
            ),
            Hero(
              tag: 'logo',
              child: Container(
                height: 200.0,
                child: Image.asset('images/logo.png'),
              ),
            ),
            SizedBox(
              height: 48.0,
            ),
            StreamBuilder(
              stream: db.streamStations(loggedInUser),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  Center(
                    child: CircularProgressIndicator(
                      backgroundColor: Colors.teal[300],
                    ),
                  );
                }
                List<Station> stationList = snapshot.data;
                if (stationList.length > 0) {
                  return Expanded(
                    child: ListView.builder(
                      itemCount: stationList.length,
                      itemBuilder: (context, index) {
                        Station station = stationList[index];
                        return StreamBuilder(
                          stream: db.streamDatapoint(station),
                          builder: (context, snapshot) {
                            if (!snapshot.hasData) {
                              return SizedBox(
                                height: 10.0,
                              );
                            }
                            Datapoint datapoint = snapshot.data;
                            return RoundedButton(
                                buttonColor: Colors.teal[300],
                                buttonText: datapoint.data['location'],
                                onPressed: () async {
                                  //get API for animation
                                  var weatherData = await WeatherModel()
                                      .getCityWeather(
                                          datapoint.data['location']);
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => ActualWeather(
                                          station: station,
                                          weatherData: weatherData,
                                          datapoit: datapoint,
                                          mapCapability: mapCapability),
                                    ),
                                  );
                                });
                          },
                        );
                      },
                    ),
                  );
                } else {
                  return Expanded(
                    child: Text(
                        'Sie haben noch keine Station, warten Sie bitte auf eine Rückmeldung!'),
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
