import 'package:flutter/material.dart';
import 'package:luftgut/components/weatherCard.dart';
import 'package:luftgut/screens/reports_screen.dart';
import 'package:luftgut/services/firebase/database.dart';
import 'package:luftgut/services/firebase/databaseService.dart';
import 'package:luftgut/services/openWeather/weatherAnimation.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

final db = DatabaseService();

class ActualWeather extends StatefulWidget {
  static const String id = 'actual_weather_screen';

  ActualWeather(
      {this.weatherData, this.station, this.mapCapability, this.datapoit});

  final weatherData;
  final station;
  final mapCapability;
  final datapoit;

  @override
  ActualWeatherState createState() => ActualWeatherState(
      weatherData: weatherData,
      station: station,
      mapCapability: mapCapability,
      datapoit: datapoit);
}

class ActualWeatherState extends State<ActualWeather> {
  ActualWeatherState(
      {this.weatherData, this.station, this.mapCapability, this.datapoit});

  final weatherData;
  final station;
  final datapoit;
  final mapCapability;
  String weatherJsonIcon;

  String getWeatherAnimation() {
    int condition = weatherData['weather'][0]['id'];
    int sunrise = weatherData['sys']['sunrise'];
    int sunset = weatherData['sys']['sunset'];
    int now = weatherData['dt'];

    return WeatherModel().getAnimation(condition, sunrise, sunset, now);
  }

  @override
  void initState() {
    weatherJsonIcon = getWeatherAnimation();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: null,
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.bar_chart),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ReportsScreen(
                      station: station,
                      mapCapability: mapCapability,
                      datapoint: datapoit,
                    ),
                  ),
                );
              }),
          // IconButton(icon: Icon(Icons.settings_sharp), onPressed: () {}),
        ],
        title: Text('⛅ Luftgut'),
        backgroundColor: Colors.teal[300],
      ),
      backgroundColor: Color(0xFFdef1f9),
      body: SafeArea(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 30.0,
            width: 150.0,
          ),
          Lottie.asset(weatherJsonIcon,
              width: 250.0, height: 250.0, fit: BoxFit.fill),
          //Text(weatherIcon, style: kIconTextStyle),
          SizedBox(
            height: 20.0,
            width: 150.0,
            child: Divider(
              color: Colors.teal[100],
            ),
          ),
          MultiProvider(
            providers: [
              StreamProvider<Datapoint>.value(
                  value: db.streamDatapoint(station)),
            ],
            child: DatapointCardList(
                datapoint: datapoit, mapCapability: mapCapability),
          ),
          //StreamWeather(weatherStation: weatherStation),
        ],
      )),
    );
  }
}

class DatapointCardList extends StatelessWidget {
  final Datapoint datapoint;
  final mapCapability;

  DatapointCardList({this.datapoint, this.mapCapability});

  @override
  Widget build(BuildContext context) {
    if (datapoint != null) {
      List<WeatherCard> weatherCardList = [];

      //Location
      WeatherCard weatherCardLocation = new WeatherCard(
        weatherText: datapoint.data['location'],
        capability: mapCapability['location'],
      );
      weatherCardList.add(weatherCardLocation);

      //Datapoints
      Map<String, dynamic> dataList = datapoint.data;
      if (dataList.isNotEmpty) {
        for (String key in datapoint.data.keys) {
          if (key != 'location') {
            WeatherCard weatherCard = new WeatherCard(
              weatherText: datapoint.data[key].round().toString(),
              capability: mapCapability[key.toString()],
            );
            weatherCardList.add(weatherCard);
          }
        }
      }
      if (weatherCardList.length > 0) {
        return Expanded(
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
            children: weatherCardList,
          ),
        );
      }
      return Expanded(
        child: Text(
            'Sie haben noch keine Aktualisierung, warten Sie bitte auf eine Rückmeldung!'),
      );
    }
    return CircularProgressIndicator(
      valueColor: AlwaysStoppedAnimation<Color>(Colors.teal[300]),
      strokeWidth: 2,
    );
  }
}
