import 'package:flutter/material.dart';
import 'package:luftgut/services/firebase/database.dart';
import 'package:luftgut/services/firebase/streamReports.dart';

class ShowReportScreen extends StatefulWidget {
  static const String id = 'temperatureReport_screen';

  ShowReportScreen({this.station, this.capability});

  final Station station;
  final capability;

  @override
  _ShowReportScreenState createState() => _ShowReportScreenState(
      station: station, capability: capability);
}

class _ShowReportScreenState extends State<ShowReportScreen> {
  _ShowReportScreenState({this.station, this.capability});

  final Station station;
  final capability;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: null,
        title: Text('⛅ Luftgut'),
        backgroundColor: Colors.teal[300],
      ),
      backgroundColor: Colors.teal[150],
      body: StreamReports(
        station: station,
        capability: capability,
      ),
    );
  }
}
